import React from 'react'

const DropDown = (props) => {
    console.log(props.data)
    return (
        <React.Fragment>
            <div className={props.classWrapper}>
                <label className="form-label">{props.label}</label>
                <select className="form-select" id={props.id} >
                    {props.data.map((item, key)=>(
                       <option value={item.id} key={key} selected={props.selectedId===item.id ? true : false}>{item.name}</option>
                    ))}

                </select>
            </div>


        </React.Fragment>
    )
}

export default DropDown