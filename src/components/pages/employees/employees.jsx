import React, { useState, useEffect } from "react"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faTrash, faEdit, faHome, faBed, faList, faPeopleArrows, faTable, faSearch } from '@fortawesome/free-solid-svg-icons';
import Pagination from "react-bootstrap-4-pagination"
import EmployeesTable from "./employeestable";
import EmployeesForm from "./employeesform";
import SidebarToggler from "../../sidebartoggler"
import { listEmployees } from "../../../actions/employeeactions";
import { useDispatch, useSelector } from 'react-redux'
const Employees = (props) => {
  const [modalShow, setModalShow] = React.useState(false);
  const [isUpdating, setIsUpdating] = useState(false)
const [id,setId]= useState('');

  const handleAdd = () => {
    setIsUpdating(false)
    setModalShow(true)
  }
  
const dispatch= useDispatch()

  useEffect(()=>{
    dispatch(listEmployees())

  },[])
  return (
    <>
      <div id="page-content-wrapper">
      <SidebarToggler />
        <div class="container-fluid">
        <div className="table-header ">
          <button onClick={handleAdd} className="btn addButton  mb-2 mt-2" id='addButton'>Add Employee</button>
          <div className="search-bar"><input clasName="search" placeholder="Search" type="text" /></div>
        </div>
          <EmployeesForm
            show={modalShow}
            onHide={() => setModalShow(false)}
            isUpdating={isUpdating}
            setIsUpdating={setIsUpdating}
            id={id}
          />
          <EmployeesTable
            isUpdating={isUpdating}
            setIsUpdating={setIsUpdating}
            setModalShow={setModalShow}
            setId={setId}
          />
        </div>
      </div>
    </>
  )
}
export default Employees;