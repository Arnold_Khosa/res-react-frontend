import React, { useState, useEffect } from "react"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faTrash, faEdit, faHome, faBed, faList, faPeopleArrows, faTable, faSearch } from '@fortawesome/free-solid-svg-icons';

import TableFooter from "../../pagination";
import { deleteRoom,listRooms,addRoom,updateRoom } from "../../../actions/roomactions";
import { useDispatch, useSelector } from 'react-redux'
const RoomsTable = (props) => {
  // We start with an empty list of items.
  const [currentItems, setCurrentItems] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [itemsPerPage, setItemsPerPage] = useState(5);
  const dispatch = useDispatch();
  //for modal
  const handleClick = () => {
      props.handleEdit();
      props.setModalShow(true)
  }
 const { roomList } = useSelector((state) => state.rooms)

  const deleteHandle = (_id) => {
      dispatch(deleteRoom(_id))
      dispatch(listRooms())
  }
  const setDefaults = (data) => {
      //getting names of properties of object
      const names = Object.keys(data)
      names.forEach((name) => {
          var input = document.getElementsByName(name);
          if (input[0]) {
              input[0].defaultValue = data[name]
          }
          props.setId(data._id)
      })
  }
  const editHandle = (object) => {
     
    
      props.setModalShow(true)

      setTimeout(()=>{
      setDefaults(object)
      }
      ,150)
      
  }
  useEffect(
      () => {
          dispatch(listRooms())
      }, [dispatch]
  )
    return (
        <>          <div className="tableData">
            <table className="table table-hover ">
                <thead className=" text-light">
                    <th>
                        #
                    </th>
                    <th>
                        Room number
                    </th>
                    <th>
                        room Key
                    </th>
                    <th>
                        Room Price
                    </th>
                    <th>
                        Room owner
                    </th>
                    <th>
                        Room description
                    </th>
                    <th>
                        Room Type
                    </th>
                    <th>
                        Room status
                    </th>
                    <th style={{ width: "40px", textAlign: "center" }}>
                        Action
                    </th>
                </thead>
                <tbody>
                {currentItems ?
                            currentItems.map((item, key) => (
                        <tr key={key}>
                            <td>
                               {key+1}
                            </td>
                            <td>
                               {item.roomNumber}
                            </td>
                            <td>
                            {item.roomKey}
                            </td>
                            <td>
                            {item.roomPrice}
                            </td>
                            <td>
                            {item.roomOwner}
                            </td>
                            <td>
                            {item.roomDescription}<button className="btn btn-secondary pt-0 pb-0 ms-1 view-room-details">view</button>
                            </td>
                            <td>
                            {item.roomType}
                            </td>
                            <td>
                            {item.roomStatus}
                            </td>
                            <td className="d-flex border ">
                                        <button className="ms-0" onClick={() => {
                                            editHandle(item)
                                            props.setIsUpdating(true)
                                        }} className="ms-auto ps-3 pe-3 border  text-danger"><FontAwesomeIcon icon={faEdit} /></button>
                                        <button onClick={() => {
                                            deleteHandle(item._id)
                                        }} className="ms-0  ps-3 pe-3 border text-primary"><FontAwesomeIcon icon={faTrash} /></button>
                                    </td>
                        </tr>
                    )):''}
                </tbody>
            </table>
        </div>
          
        {roomList && <TableFooter
                data={roomList}
                setPageCount={setPageCount}
                setCurrentItems={setCurrentItems}
                itemsPerPage={itemsPerPage}
                currentItems={currentItems}
                pageCount={pageCount}
                currentItems={currentItems}

            />
            }
        </>
    )
}
export default RoomsTable;
