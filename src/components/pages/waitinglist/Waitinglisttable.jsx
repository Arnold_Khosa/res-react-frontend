import React, { useState, useEffect } from "react"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash, faEdit} from '@fortawesome/free-solid-svg-icons';
import TableFooter from "../../pagination";
import {listWaitingList,deleteWaitingListItem} from "../../../actions/waitinglistactions";
import { useDispatch, useSelector } from 'react-redux'
const WaitinglistTable = (props) => {
  // We start with an empty list of items.
  const [currentItems, setCurrentItems] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [itemsPerPage, setItemsPerPage] = useState(5);
  const dispatch = useDispatch();

  const { waitingList} = useSelector((state) => state.waitingList)
  
  const deleteHandle = (_id) => {
    dispatch(deleteWaitingListItem(_id))
    dispatch(listWaitingList())
  }
  const setDefaults = (data) => {
    //getting names of properties of object
    const names = Object.keys(data)
    names.forEach((name) => {
      var input = document.getElementsByName(name);
      if (input[0]) {
        input[0].defaultValue = data[name]
      }
      props.setId(data._id)
    })
   
  }
  const editHandle = (object) => {
    props.setModalShow(true)
    setTimeout(() => {
      setDefaults(object)
    }
      , 250)

  }
  useEffect(
    () => {
      dispatch(listWaitingList())
    }, [dispatch]
  )
  return (
    <>
      <div className="tableData">
        <table className="table table-hover ">
          <thead className=" text-light">
            <th>
              #
            </th>
            <th>
              Name
            </th>
            <th>
              Surname
            </th>
            <th>
              Cell Number
            </th>
            <th>
              email
            </th>
            <th>
              Room type requested
            </th>
            <th>
              Upfront Payment
            </th>
            <th style={{ width: "40px", textAlign: "center" }}>
              Action
            </th>
          </thead>
          <tbody>
            {currentItems ?
              currentItems.map((item, key) => (
                <tr key={key}>
                  <td>
                    {key + 1}
                  </td>
                  <td>
                   {item.name}
                  </td>
                  <td>
                   {item.surname}
                  </td>
                  <td>
                  {item.contact}
                  </td>
                  <td>
                  {item.email}
                  </td>
                  <td>
                 {item.roomTypeRequested}
                  </td>

                  <td>
                   R{item.upfrontPayment}
                  </td>
                  <td className="d-flex border ">
                    <button  className="ms-auto ps-3 pe-3 border  text-danger" onClick={() => {
                      editHandle(item);
                     props.setIsUpdating(true);
                    }} 
                     ><FontAwesomeIcon icon={faEdit} />
                    </button>
                    <button onClick={() => {
                      deleteHandle(item._id)
                    }} className="ms-0  ps-3 pe-3 border text-primary"><FontAwesomeIcon icon={faTrash} /></button>
                  </td>
                </tr>
              )
              ) : ''}
          </tbody>
        </table>
      </div>

      {waitingList && <TableFooter
        data={ waitingList}
        setPageCount={setPageCount}
        setCurrentItems={setCurrentItems}
        itemsPerPage={itemsPerPage}
        pageCount={pageCount}
        currentItems={currentItems}

      />
      }
    </>
  )
}
export default WaitinglistTable