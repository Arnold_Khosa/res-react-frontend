import React, { useState, useEffect } from "react"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faTrash, faEdit, faHome, faBed, faList, faPeopleArrows, faTable, faSearch } from '@fortawesome/free-solid-svg-icons';
import data from '../../data'
import { useSelector, useDispatch } from 'react-redux'
import { listTenants, deleteTenant } from "../../../actions/tenantactions";
import ReactPaginate from "react-paginate";
import TableFooter from "../../pagination";
const TenantsTable = (props) => {
    // We start with an empty list of items.
    const [currentItems, setCurrentItems] = useState([]);
    const [pageCount, setPageCount] = useState(0);
    const [itemsPerPage, setItemsPerPage] = useState(10);
    const dispatch = useDispatch();
    //for modal
    const handleClick = () => {
        props.handleEdit();
        props.setModalShow(true)
    }
    const { tenantList } = useSelector((state) => state.tenants)

    const deleteHandle = (_id) => {
        dispatch(deleteTenant(_id))
        dispatch(listTenants())
    }
    const setDefaults = (data) => {
        //getting names of properties of object
        const names = Object.keys(data)
        names.forEach((name) => {
            var input = document.getElementsByName(name);
            if (input[0]) {
                input[0].defaultValue = data[name]
            }
            props.setId(data._id)
        })
       
    }
    const editHandle = (object) => {
        props.setModalShow(true)
        setTimeout(() => {
          setDefaults(object)
        }
          , 250)
    
      }
    useEffect(
        () => {
            dispatch(listTenants())
        }, [dispatch]
    )
    return (
        <>
            <div className="tableData">
                <table className=" table table-hover ">
                    <thead className=" text-light">
                        <th>
                            #
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            Surname
                        </th>
                        <th>
                            Contacts
                        </th>
                        <th>
                            Date Moved In
                        </th>
                        <th>
                            Room
                        </th>
                        <th>
                            Due Pay
                        </th>
                        <th style={{ width: "40px", textAlign: "center" }}>
                            Action
                        </th>
                    </thead>
                    <tbody>
                        {currentItems ?
                            currentItems.map((item, key) => (
                                <tr key={key}>
                                    <td>
                                        {key + 1}
                                    </td>
                                    <td>
                                        {item.name}
                                    </td>
                                    <td>
                                        {item.surname}
                                    </td>
                                    <td>
                                        {item.contact}
                                    </td>
                                    <td>
                                        {item.dateMovedIn}
                                    </td>
                                    <td className="d-flex">
                                        {item.roomNumber}<button className="btn ms-auto btn-secondary pt-0 pb-0 ms-1 view-room-details">view</button>
                                    </td>
                                    <td>
                                        R{item.duePay}
                                    </td>
                                    <td className="d-flex border ">
                                        <button onClick={() => {
                                            editHandle(item)
                                            props.setIsUpdating(true);
                                        }} className="ms-auto ps-3 pe-3 border  text-danger"><FontAwesomeIcon icon={faEdit} /></button>
                                        <button onClick={() => {
                                            deleteHandle(item._id)
                                        }} className="ms-0  ps-3 pe-3 border text-primary"><FontAwesomeIcon icon={faTrash} /></button>
                                    </td>
                                </tr>
                            )
                            ) :(
                                <h1>Loading</h1>
                            ) 
                            }
                    </tbody>
                </table>
            </div>

            {tenantList && <TableFooter
                data={tenantList}
                setPageCount={setPageCount}
                setCurrentItems={setCurrentItems}
                itemsPerPage={itemsPerPage}
                currentItems={currentItems}
                pageCount={pageCount}

            />
            }
        </>

    )
}
export default TenantsTable