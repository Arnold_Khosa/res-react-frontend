import { BrowserRouter, BrowserRouter as Router, Link, NavLink, Route, Switch,Redirect } from "react-router-dom"
import React from "react"
import { useDispatch, useSelector } from 'react-redux'
const PrivateRoute = ({children, authenticated, ...rest}) =>{
const {userInfo}=  useSelector((state)=>state.userSignin)
return (
<Route {...rest} render={()=>{
    return userInfo && !userInfo.timeout? children: <Redirect 
    to='/login'
       />
     }}
    
/>
)
}
export default PrivateRoute;
