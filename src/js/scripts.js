
window.addEventListener('DOMContentLoaded', event => {

    // Toggle the side navigation
    const button = document.body.querySelector('.toggle');
    const nav = document.body.querySelector('.topnav');

    if (button) {
       
        button.addEventListener('click', event => {
               nav.classList.toggle('collapse')
        });
    }

});
