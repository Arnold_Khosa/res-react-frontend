import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {PieChart} from "react-minimal-pie-chart"
import { Link } from "react-router-dom";

const Error = () =>{
    const style={
        flex:1,
        border:"none",
        borderBottom:"1px solid light",
  backgroundColor:"#20232E"
    }
    return (
        <>
    <h1>404! The page requested does not exist</h1>
    <Link to="/"> Return to home screen</Link>
    
        </>
    )
}
export default Error;
