import React, { useState, useEffect } from "react"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faTrash, faEdit, faHome, faBed, faList, faPeopleArrows, faTable, faSearch } from '@fortawesome/free-solid-svg-icons';
import Pagination from "../../pagination"
import { useDispatch, useSelector } from 'react-redux'
import TenantsTable from "./tenantstable";
import TenantsForm from "./tenantsform";
import SidebarToggler from "../../sidebartoggler"

import { listTenants } from "../../../actions/tenantactions";

const Tenants = (props) => {
  const [modalShow, setModalShow] = React.useState(false);
  const [isUpdating, setIsUpdating] = useState(false)
  const [id, setId] = useState("");

  const handleAdd = () => {
    setIsUpdating(false)
    setModalShow(true)
  }

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(listTenants())

  }, [])
  return (
    <> <div id="page-content-wrapper">
      <SidebarToggler />
      <div className="container-fluid">
        <div className="table-header ">
          <button onClick={handleAdd} className="btn addButton  mb-2 mt-2" id='addButton'>Add Tenant</button>
          <div className="search-bar"><input clasName="search" placeholder="Search" type="text" /></div>
        </div>
        <TenantsForm
          show={modalShow}
          onHide={() => setModalShow(false)}
          isUpdating={isUpdating}
          setIsUpdating={setIsUpdating}
          id={id}
        />
        <TenantsTable
          isUpdating={isUpdating}
          setIsUpdating={setIsUpdating}
          setModalShow={setModalShow}
          setId={setId}
        />
      </div>
      {/* <Pagination/> */}
    </div>
    </>
  )
}
export default Tenants;