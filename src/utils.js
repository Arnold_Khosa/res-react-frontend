
import axios from "axios"
import jwt_decode from "jwt-decode";
import {useDispatch} from 'react-redux'
export const verifyToken = async () => {
    try{
        const userInfo = JSON.parse(localStorage.getItem('userInfo'));
        let currentDate = new Date();
        const decodedToken = jwt_decode(userInfo.accessToken)
        if (decodedToken.exp * 1000 < currentDate.getTime()) {
            const data = await axios.post('http://localhost:5000/refresh',
                { token: userInfo.refreshToken })
            localStorage.setItem('userInfo', JSON.stringify(data.data))
            console.log(data.data)
            console.log('refresh')
        }
    }
    catch (error) {
    
        if(error.timeout){
         localStorage.removeItem('userInfo')
        console.log('hey')
        }
    }
}

