import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import ReactPaginate from 'react-paginate';

const TableFooter = (props) => {
    // Here we use item offsets; we could also use page offsets
    // following the API or data you're working with.
    const [itemOffset, setItemOffset] = useState(0);

    useEffect(() => {
        // Fetch items from another resources.
        const endOffset = itemOffset + props.itemsPerPage;
        console.log(`Loading items from ${itemOffset} to ${endOffset}`);
        props.setCurrentItems(props.data.slice(itemOffset, endOffset));
        props.setPageCount(Math.ceil(props.data.length / props.itemsPerPage));
    }, [itemOffset, props.itemsPerPage, props.data]);

    // Invoke when user click to request another page.
    const handlePageClick = (event) => {
        const newOffset = (event.selected * props.itemsPerPage) % props.data.length;
        console.log(
            `User requested page number ${event.selected}, which is offset ${newOffset}`
        );
        setItemOffset(newOffset);
    };

    return (
        <>
<div className='footer'>
    <hr/>
            <span className="records-per-page">
                <select >
                    <option>show 10 records</option>
                    <option>show 15 records</option>
                    <option>show 20 records</option>
                    <option>show 25 records</option>
                    <option>show 30 records</option>
                    <option>show 35 records</option>
                    <option>show 40 records</option>
                </select>
            </span>
            <span className=" paginate">
                <ReactPaginate
                    breakLabel="..."
                    nextLabel="Next"
                    onPageChange={handlePageClick}
                    pageRangeDisplayed={3}
                    pageCount={props.pageCount}
                    previousLabel="Prev"
                    renderOnZeroPageCount={null}

                    className="pagination-container"
                    pageClassName="page"
                    activeClassName="active"
                    previousLinkClassName="previous"
                    nextLinkClassName="next"
                    disabledClass="disabled"
                />
            </span>
            </div>

        </>
    )
}
export default TableFooter;
