import {useState} from "react";
const NumberInput = (props) => {
    const [value, setValue] = useState();
    return (
        <>
            <div className={props.classWrapper}>
                <label className="form-label">
                    {props.label}
                </label>
                <input type="number"
                    className="form-control"
                    id={props.id}
                    placeholder={props.placeholder}
                    max={props.max}
                    min={props.min}
                    value={props.value}
                    onChange={(event) => {
                        setValue(event.target.value)
                    }}
                />
            </div>
        </>
    )
}
export default NumberInput;