
import {EMPLOYEE_LIST_REQUEST,EMPLOYEE_LIST_SUCCESS,EMPLOYEE_LIST_FAIL} from '../constants/employeeconstants'
export const employeeListReducer = (state={employeeList:[],loading:true}, action) => {
    switch (action.type) {
        case EMPLOYEE_LIST_REQUEST:
            return { loading: true }

        case EMPLOYEE_LIST_SUCCESS:
            return {employeeList: action.payload }

        case EMPLOYEE_LIST_FAIL:
            return { loading: false, error: action.payload }
        default:
            return state;
    }
}