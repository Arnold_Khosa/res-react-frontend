import { Link, useLocation } from "react-router-dom";
import { Modal, Button } from 'react-bootstrap';
import Input from "../../widgets/form/input";
import react from "react"
import { addToWaitingList,listWaitingList,updateWaitingListItem,deleteWaitingListItem} from "../../../actions/waitinglistactions";
import { useDispatch, useSelector } from 'react-redux'
const WaitinglistForm = (props) => {
    var {id}=props;
    const dispatch = useDispatch();

    const handleSubmit = (e) => {
        e.preventDefault();
        var formData = {}
        for (var i = 0; i < e.target.length; i++) {
            if (e.target[i].name !== "") {
                const name = e.target[i].name;
                const value = e.target[i].value;
                formData[name] = value;
            }
        }
        if (props.isUpdating === false) {
            dispatch(addToWaitingList(formData))
        }
        else {
            dispatch(updateWaitingListItem({formData,id}))
        }
      dispatch(listWaitingList())
        props.onHide();
    }
    return (
        <>
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        {props.isUpdating === true ? "Update List Item Details" : "Add New Person To the List"}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>

                    <form className="mb-2 me-3" onSubmit={handleSubmit}>
                        <div className="form-row">
                            <Input
                                label={"Name"}
                                placeholder={"Name"}
                                name="name"
                                type="text"
                                required
                            />
                            <Input
                                label={"Surname"}
                                placeholder={"Surname"}
                                name="surname"
                                type="text"
                                required
                            />
                            <Input
                                label={"Phone Number"}
                                placeholder={"phone Number"}
                                name="contact"
                                type="text"
                                required
                            />
                            <Input
                                label={"Email"}
                                placeholder={"Email"}
                                name="email"
                                type="email"
                                required
                            />
                            <Input
                                label={"Upfront Payment"}
                                placeholder={"Upfront Payment"}
                                name="upfrontPayment"
                                required
                                type="number"
                            />
                            <Input
                                label={"Room Type Requested"}
                                placeholder={"Room Type Requested"}
                                name="roomTypeRequested"
                                type="text"
                                required
                            />
                        </div>
                        <Modal.Footer>
                            <Button onClick={props.onHide} type="button" className="btn btn-danger">close </Button>
                            <Button type="submit" style={{ backgroundColor: "#23282D" }} className="btn">Submit</Button>
                        </Modal.Footer>
                    </form>
                </Modal.Body>
            </Modal>
        </>
    )
}
export default WaitinglistForm