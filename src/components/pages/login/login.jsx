import { BrowserRouter, BrowserRouter as Router, Link, NavLink, Route, Switch, Redirect, useHistory } from "react-router-dom"
import React, { useState, useEffect } from "react"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faTrash, faEdit, faUser, faEyeSlash, faLock, faTools, faHome, faBed, faList, faPeopleArrows, faTable, faSearch } from '@fortawesome/free-solid-svg-icons';
import axios from "axios";
import jwt_decode from "jwt-decode";
import { useDispatch, useSelector } from 'react-redux'
import { signIn } from "../../../actions/useractions";

const Login = (props) => {
    const [user, setUser] = useState({
        username: "",
        password: ""
    });
    const { userInfo } = useSelector((state) => state.userSignin)
    const history = useHistory()

    const dispatch = useDispatch();
    if (userInfo && !userInfo.timeout) {
        history.push('/')
    }
    const submitHandler = (e) => {
        e.preventDefault()
        dispatch(signIn(user))

    }
    return (
        <>
          
            <div className="login">
                <div className="login-container  ">
                    <div className="panel">
                        <div className="panel-heading">
                            <h3 className="pt-3 font-weight-bold text-white">Login</h3>
                        </div>
                        <div className="panel-body  p-3">


                            <form onSubmit={submitHandler}>
                                <div className="form-group py-1 pb-2 pt-3">
                                <label  className="login-labels" >Username</label>
                                    <div className="input-field"> 
                                    <span style={{color:'gold'}} className="fas fa-lock p-1"><FontAwesomeIcon icon={faUser} />
                                    </span>
                                        <input required onChange={(event) => (
                                            setUser({ ...user, username: event.target.value })
                                        )} type="text" placeholder="Enter your username" required /> <button className="btn bg-white text-muted"> </button> </div>
                                </div>
                                <label  className="login-labels" >Password</label>
                                <div className="form-group py-1 pb-2">
                                    <div className="input-field"> 
                                    <span  style={{color:'gold'}} className="fas fa-lock text-alert p-1"><FontAwesomeIcon icon={faLock} />
                                    </span> <input required onChange={(event) => (
                                        setUser({ ...user, password: event.target.value })
                                    )} type="password" placeholder="Enter your Password" required /> <button className="btn bg-white text-muted"> <span className="far fa-eye-slash"><FontAwesomeIcon icon={faEyeSlash} /></span> </button> </div>
                                </div>
                                <div className="form-inline m-3"> <a href="#" id="forgot" className="font-weight-bold"></a> </div>
                                <div classNameName="d-block ">
                                    <button type="submit" className="btn login-button btn-primary
                                    btn-block d-block ">Login</button></div>
                            </form>


                        </div>
                    </div>
                </div>

            </div>
        </>
    )
}
export default Login;