import React, { useState, useEffect } from "react"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faTrash, faEdit, faHome, faBed, faList, faPeopleArrows, faTable, faSearch } from '@fortawesome/free-solid-svg-icons';
import TableFooter from "../../pagination";
import { deleteEmployee, listEmployees, addEmployee, updateEmployee } from "../../../actions/employeeactions";
import { useDispatch, useSelector } from 'react-redux'
const EmployeesTable = (props) => {
  // We start with an empty list of items.
  const [currentItems, setCurrentItems] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [itemsPerPage, setItemsPerPage] = useState(5);
  const dispatch = useDispatch();
  //for modal
  const handleClick = () => {
    props.handleEdit();
    props.setModalShow(true)
  }
  const { employeeList } = useSelector((state) => state.employees)
  const deleteHandle = (_id) => {
    dispatch(deleteEmployee(_id))
    dispatch(listEmployees())
  }
  const setDefaults = (data) => {
    //getting names of properties of object
    const names = Object.keys(data)
    names.forEach((name) => {
      var input = document.getElementsByName(name);
      if (input[0]) {
        input[0].defaultValue = data[name]
      }
      props.setId(data._id)
    })
  }
  const editHandle = (object) => {
    props.setModalShow(true)
    setTimeout(() => {
      setDefaults(object)
    }
      , 150)
  }
  useEffect(
    () => {
      dispatch(listEmployees())
    }, [dispatch]
  )
  return (
    <>
      <div className="tableData">
        <table className="table table-hover ">
          <thead className=" text-light">
            <th>
              #
            </th>
            <th>
              Name
            </th>
            <th>
              Surname
            </th>
            <th>
              Cell number
            </th>
            <th>
              Date Hired
            </th>
            <th>
              Date contract end
            </th>
            <th>
              Job Description
            </th>
            <th>
              Salary
            </th>
            <th style={{ width: "40px", textAlign: "center" }}>
              Action
            </th>
          </thead>
          <tbody>
            {currentItems ?
              currentItems.map((item, key) => (
                <tr key={key}>
                  <td>
                    {key + 1}
                  </td>
                  <td>
                    {item.name}
                  </td>
                  <td>
                    {item.surname}
                  </td>
                  <td>
                    {item.contact}
                  </td>
                  <td>
                    {item.startDate}
                  </td>
                  <td>
                    {item.endDate}
                  </td>

                  <td>
                    {item.jobDescription} <button className="btn btn-secondary pt-0 pb-0 ms-1 view-room-details">view</button>
                  </td>
                  <td>
                    R{item.salary}
                  </td>
                  <td className="d-flex border ">
                    <button onClick={() => {
                      editHandle(item)
                      props.setIsUpdating(true);
                    }} className="ms-auto ps-3 pe-3 border  text-danger"><FontAwesomeIcon icon={faEdit} /></button>
                    <button onClick={() => {
                      deleteHandle(item._id)
                    }} className="ms-0  ps-3 pe-3 border text-primary"><FontAwesomeIcon icon={faTrash} /></button>
                  </td>
                </tr>
              )) : ''}
          </tbody>
        </table>
      </div>

      {employeeList && <TableFooter
        data={employeeList}
        setPageCount={setPageCount}
        setCurrentItems={setCurrentItems}
        itemsPerPage={itemsPerPage}
        pageCount={pageCount}
        currentItems={currentItems}

      />
      }
    </>
  )
}
export default EmployeesTable