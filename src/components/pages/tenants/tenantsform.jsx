import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faWindowClose, faSav } from '@fortawesome/free-solid-svg-icons';
import { Link, useLocation } from "react-router-dom";
import { Modal, Button } from 'react-bootstrap';
import Input from "../../widgets/form/input";
import { useDispatch, useSelector } from 'react-redux';
import { addTenant, listTenants, updateTenant } from '../../../actions/tenantactions';

const TenantsForm = (props) => {
    var {id}=props;
    const dispatch = useDispatch();
    const handleSubmit = (e) => {
        e.preventDefault();
        var formData = {}

        for (var i = 0; i < e.target.length; i++) {
            if (e.target[i].name !== "") {
                const name = e.target[i].name;
                const value = e.target[i].value;
                formData[name] = value;
            }
        }
        if (props.isUpdating === false) {
            dispatch(addTenant(formData))
        }
        else {
            dispatch(updateTenant({formData,id}))
        }
        dispatch(listTenants())
        props.onHide();
    }
    return (
        <>
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        {props.isUpdating === true ? "Update Tenant Details" : "Add New Tenant"}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>

                    <form className="mb-2 me-3" onSubmit={handleSubmit}>
                        <div className="form-row">
                            <Input
                                label={"Name"}
                                placeholder={"Name"}
                                name="name"
                                type="text"
                                required
                            />
                            <Input
                                label={"Surname"}
                                placeholder={"Surname"}
                                name="surname"
                                type="text"
                                required
                            />
                            <Input
                                label={"Phone Number"}
                                placeholder={"phone Number"}
                                name="contact"
                                type="text"
                                required
                            />
                            <Input
                                label={"Date Moved In"}
                                placeholder={"Date Moved In"}
                                name="dateMovedIn"
                                type="date"
                                required
                            />
                            <Input
                                label={"Due Pay"}
                                placeholder={"Due Pay"}
                                name="duePay"
                                type="text"
                                required
                            />
                            {/* room room number use" room.roomNumber" */}
                            <Input
                                label={"Room Number"}
                                placeholder={"Room Number"}
                                name="roomNumber"
                                type="text"
                            />
                        </div>
                        <Modal.Footer>
                            <Button onClick={props.onHide} type="button" className="btn btn-danger">close </Button>
                            <Button type="submit" style={{ backgroundColor: "#23282D" }} className="btn">Submit</Button>
                        </Modal.Footer>
                    </form>
                </Modal.Body>

            </Modal>

        </>
    )
}
export default TenantsForm