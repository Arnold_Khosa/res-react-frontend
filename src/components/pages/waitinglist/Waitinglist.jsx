import React, { useState, useEffect } from "react"
import WaitinglistTable from "./Waitinglisttable";
import WaitinglistForm from "./Waitinglistform";
import SidebarToggler from "../../sidebartoggler"
import { listWaitingList } from "../../../actions/waitinglistactions";
import { useDispatch } from 'react-redux'
const WaitingList = (props) => {
  const [modalShow, setModalShow] = React.useState(false);
  const [isUpdating, setIsUpdating] = useState(false)
  const [id, setId] = useState("");

  const handleAdd = () => {
    setIsUpdating(false)
    setModalShow(true)
  }
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(listWaitingList())
  }, [])
  return (
    <>
      <div id="page-content-wrapper">
        <SidebarToggler />
        <div class="container-fluid">
          <div className="table-header ">
            <button onClick={handleAdd} className="btn addButton  mb-2 mt-2" id='addButton'>Add to waiting list</button>
            <div className="search-bar"><input clasName="search" placeholder="Search" type="text" /></div>
          </div>
          <WaitinglistForm
            show={modalShow}
            onHide={() => setModalShow(false)}
            isUpdating={isUpdating}
            setIsUpdating={setIsUpdating}
            id={id}
          />
          <WaitinglistTable
            isUpdating={isUpdating}
            setIsUpdating={setIsUpdating}
            setModalShow={setModalShow}
            setId={setId}
          />
        </div>
      </div>
    </>
  )
}
export default WaitingList;