
import Axios from 'axios'
import { USER_SIGNIN_REQUEST, USER_SIGNIN_SUCCESS, USER_SIGNIN_FAIL, USER_SIGNOUT } from '../constants/userconstants'
import { USER_REGISTER_REQUEST, USER_REGISTER_SUCCESS, USER_REGISTER_FAIL} from '../constants/userconstants'
import {USER_REFRESH_REQUEST, USER_REFRESH_SUCCESS, USER_REFRESH_FAIL } from '../constants/userconstants'

const instance= Axios.create();

export const signIn = ({username, password}) => async (dispatch) => {
    dispatch({
        type: USER_SIGNIN_REQUEST,
        payload: {
            username,
            password
        }
    })
    try {
        const { data } = await Axios.post('/login', {username, password })
        dispatch({
            type: USER_SIGNIN_SUCCESS,
            payload: data
        })
        localStorage.setItem('userInfo', JSON.stringify(data))
        
    } catch (error) {
        dispatch({
            type: USER_SIGNIN_FAIL,
            payload: error.response && error.response.data.message
                ? error.response.data.message
                : error.response
        })
    }
}

export const refreshToken = (token) => async (dispatch) => {
    dispatch({
        type: USER_REFRESH_REQUEST,
        payload: {
          token
        }
    })
   
    try {
       
        instance.post('/refresh',token)
        .then(res=>{
            
        dispatch({
            type: USER_REFRESH_SUCCESS,
            payload: res.data
        })
        localStorage.setItem('userInfo', JSON.stringify(res.data))
        }).catch((error)=>{
            dispatch({
                type: USER_REFRESH_FAIL,
            })
            console.log(error)
            localStorage.removeItem('userInfo')
            document.location.reload(true);
            
        })
       
       
       
    } catch (error) {
        console.log(error)
       
    }
}














export const signOut = () => (dispatch) => {
    localStorage.removeItem('userInfo');
    dispatch({
        type: USER_SIGNOUT
    })
}
export const register = (name, email, password) => async (dispatch) => {
    dispatch({
        type: USER_REGISTER_REQUEST,
        payload: {
            name,
            email,
            password
        }
    })
    try {
        const { data } = await Axios.post('/api/users/register', {name, email, password })
        dispatch({
            type: USER_REGISTER_SUCCESS,
            payload: data
        })
        dispatch({
            type: USER_SIGNIN_SUCCESS,
            payload: data
        })
        localStorage.setItem('userInfo', JSON.stringify(data))
    } catch (error) {
        dispatch({
            type: USER_REGISTER_FAIL,
            payload: error.response && error.response.data.message
                ? error.response.data.message
                : error.response
        })
    }
}
