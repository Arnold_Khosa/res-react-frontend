import React, { Fragment } from 'react'

const RadioButton = (props) => {
    console.log(props.data)
    return (
        <React.Fragment>
            <div className={props.classWrapper}>
                <input
                    className="form-check-input"
                    type="radio"
                    name="inlineRadioOptions"
                    id="inlineRadio1"
                    value="option1"
                />
                <label className="form-label">{props.label}</label>
            </div>

        </React.Fragment>

    )
}
export default RadioButton