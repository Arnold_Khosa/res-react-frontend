import React from 'react'

const CheckBox = (props) => {
    return (
        <React.Fragment>
            <div className="btn-group" role="group" aria-label="Basic checkbox toggle button group">
                <input type="checkbox" className="btn-check" id="btncheck1" autocomplete="off" />
                    <label className="btn btn-outline-secondary" for="btncheck1">monthly</label>

                    <input type="checkbox" className="btn-check" id="btncheck2" autocomplete="off" />
                        <label className="btn btn-outline-secondary" for="btncheck2">yearly</label>

                        <input type="checkbox" className="btn-check" id="btncheck3" autocomplete="off" />
                            <label className="btn btn-outline-secondary" for="btncheck3">half yearly/ per semester</label>
                        </div>
                    </React.Fragment>
                    )
}
                    export default CheckBox