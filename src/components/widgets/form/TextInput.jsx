import { useState } from "react";
const TextInput = (props) => {
    return (
        <>
            <div style={{ padding: "1px" }} className="d-flex container">
                <label className="form-label col-3">
                    {props.label}
                </label>
                <input type="text"
                    className="form-control"
                    id={props.id}
                    id={props.name}
                    placeholder={props.placeholder}
                    max={props.max}
                    min={props.min}
                    value={props.value}

                />
            </div>
        </>
    )
}
export default TextInput;