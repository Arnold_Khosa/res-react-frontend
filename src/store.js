import { createStore, compose, applyMiddleware, combineReducers } from "redux"
import thunk from 'redux-thunk'

// import { cartReducer } from "./reducers/cartreducer"
import { tenantListReducer} from "./reducers/tenantreducers"
import { roomListReducer} from "./reducers/roomreducers"
import { employeeListReducer} from "./reducers/employeereducers"
import {  waitingListReducer} from "./reducers/waitinglistreducers"

import {userSignInReducer} from "./reducers/userreducer"


const initialState = {
   
    userSignin:{
        userInfo:localStorage.getItem('userInfo')
        ?JSON.parse(localStorage.getItem('userInfo'))
        :null
    },
}
const reducer = combineReducers({
    tenants: tenantListReducer,
    rooms:roomListReducer,
    employees: employeeListReducer, 
    waitingList:  waitingListReducer,
    userSignin:userSignInReducer,
   // userRegister:userRegisterReducer 
})
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducer,
    initialState,
    composeEnhancer(applyMiddleware(thunk))
);
export default store;