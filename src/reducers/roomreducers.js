
import {ROOM_LIST_REQUEST,ROOM_LIST_SUCCESS,ROOM_LIST_FAIL} from '../constants/roomconstants'
export const roomListReducer = (state={roomList:[],loading:true}, action) => {
    switch (action.type) {
        case ROOM_LIST_REQUEST:
            return { loading: true }

        case ROOM_LIST_SUCCESS:
            return {roomList: action.payload }

        case ROOM_LIST_FAIL:
            return { loading: false, error: action.payload }
        default:
            return state;
    }
}