const data = [{
    id: 1,
    name: 'Arnold',
    surname: 'Khosa',
    contact: '0768776644',
    dateMovedIn: '22-01-2022',
    room: '2D',
    duePay: '0'
  },
  {
    id: 2,
    name: 'Alex',
    surname: 'Mthombeni',
    contact: '0768533335',
    dateMovedIn: '02-01-2022',
    room: '5E',
    duePay: 70
  },
  {
    id: 3,
    name: 'kelly',
    surname: 'zitha',
    contact: '0768794536',
    dateMovedIn: '22-04-2021',
    room: '5H',
    duePay: 50
  },
  {
    id: 4,
    name: 'Michael',
    surname: 'dlamin',
    contact: '0768798645',
    dateMovedIn: '02-04-2021',
    room: '28',
    duePay: 0
  },
  {
    id: 5,
    name: 'Thabo',
    surname: 'Dlamini',
    contact: '0768798645',
    dateMovedIn: '02-04-2021',
    room: '19',
    duePay: 60
  },
  {
    id: 6,
    name: 'Dudu',
    surname: 'Themba',
    contact: '0768798645',
    dateMovedIn: '02-04-2021',
    room: '5',
    duePay: 340
  },
  {
    id: 7,
    name: 'Tintswalo',
    surname: 'Manzini',
    contact: '0768798645',
    dateMovedIn: '02-04-2021',
    room: '12',
    duePay: 1300
  },
  {
    id: 8,
    name: 'Xolani',
    surname: 'Jones',
    contact: '0768798645',
    dateMovedIn: '02-04-2021',
    room: '78',
    duePay: 700
  },
  {
    id: 9,
    name: 'Mandy',
    surname: 'Khosa',
    contact: '0768798645',
    dateMovedIn: '02-04-2021',
    room: '5',
    duePay: 700
  }
  ,
  {
    id: 10,
    name: 'Alton',
    surname: 'Zulu',
    contact: '0768798645',
    dateMovedIn: '02-04-2021',
    room: '5',
    duePay: 550
  },
  {
    id: 11,
    name: 'Pretty',
    surname: 'Dlamini',
    contact: '0768798645',
    dateMovedIn: '02-04-2021',
    room: '3W',
    duePay: 730
  },
  {
    id: 12,
    name: 'Sam',
    surname:'Samson',
    contact: '0768798645',
    dateMovedIn: '02-04-2021',
    room: '23',
    duePay: 700
  },
  {
    id: 13,
    name: 'Samantha',
    surname: 'Arnolds',
    contact: '0768798645',
    dateMovedIn: '02-04-2021',
    room: '5',
    duePay: 750
  },
  {
    id: 14,
    name: 'Linda',
    surname: 'Khosa',
    contact: '0768798645',
    dateMovedIn: '02-04-2021',
    room: '58',
    duePay: 0
  },
  {
    id: 15,
    name: 'Phillip',
    surname: 'Michaels',
    contact: '0768798645',
    dateMovedIn: '02-04-2021',
    room: '5',
    duePay: 50
  },
  {
    id: 16,
    name: 'Phill',
    surname: 'Zitha',
    contact: '0768798645',
    dateMovedIn: '02-04-2021',
    room: '57',
    duePay: 1000
  },
  {
    id: 17,
    name: 'Lindiwe',
    surname: 'Mnisi',
    contact: '0768798645',
    dateMovedIn: '02-04-2021',
    room: '25',
    duePay: 3400
  },
  {
    id: 18,
    name: 'Lindiwe',
    surname: 'Marimane',
    contact: '0768798645',
    dateMovedIn: '02-04-2021',
    room: '23',
    duePay: 700
  },{
    id: 19,
    name: 'Sindi',
    surname: 'Ringani',
    contact: '0768798645',
    dateMovedIn: '02-04-2021',
    room: '45',
    duePay: 770
  },
  {
    id: 20,
    name: 'Martin',
    surname: 'Mkhomazi',
    contact: '0768798645',
    dateMovedIn: '02-04-2021',
    room: '12',
    duePay: 750
  },{
    id: 21,
    name: 'Cynthia',
    surname: 'Sthole',
    contact: '0768798645',
    dateMovedIn: '02-04-2021',
    room: '2S',
    duePay: 850
  },{
    id: 22,
    name: 'Mike',
    surname: 'Kingston',
    contact: '0768798645',
    dateMovedIn: '02-04-2021',
    room: '5H',
    duePay: 1000
  }
  ]

  export default data;