// import Axios from "axios"

import Axios from 'axios'
import { ROOM_LIST_SUCCESS, ROOM_LIST_REQUEST, ROOM_LIST_FAIL } from '../constants/roomconstants'
import { ROOM_DELETE_SUCCESS, ROOM_DELETE_REQUEST, ROOM_DELETE_FAIL } from '../constants/roomconstants'
import { ROOM_EDIT_SUCCESS, ROOM_EDIT_REQUEST, ROOM_EDIT_FAIL } from '../constants/roomconstants'
import { ROOM_ADD_SUCCESS, ROOM_ADD_REQUEST, ROOM_ADD_FAIL } from '../constants/roomconstants'

export const listRooms = () => async (dispatch) => {
    dispatch({
        type: ROOM_LIST_REQUEST
    })
  
    try {
        var {data} = await Axios.get('/api/rooms')
     
       
        dispatch({
            type: ROOM_LIST_SUCCESS,
            payload: data.data
        })
    } catch (error) {
        
        dispatch({
            type: ROOM_LIST_FAIL,
            payload: error.message
        })
    }
}

export const addRoom = (room) => async (dispatch) => {
    dispatch({
        type: ROOM_ADD_REQUEST,
        payload:room
    })
 
  var data
    try {
        Axios.post('/api/rooms',room
        ).then(res=>{
      data=res;
        })
        dispatch({
            type: ROOM_ADD_SUCCESS,
            payload: data
        })
    } catch (error) {
     
        dispatch({
            type: ROOM_ADD_FAIL,
            payload: error.message
        })
    }
}

export const updateRoom = ({formData,id}) => async (dispatch) => {
    dispatch({
        type: ROOM_EDIT_REQUEST,
        payload: formData
    })
    try {
        Axios.patch(`/api/rooms/${id}`,formData).catch((error)=>{console.log(error)})

        dispatch({
            type: ROOM_EDIT_SUCCESS
        })
    } catch (error) {
       
        dispatch({
            type: ROOM_EDIT_FAIL,
            payload: error.response && error.response.data.message
                ? error.response.data.message
                : error.message,

        })
    }
}
export const deleteRoom = (roomId) => async (dispatch) => {
    dispatch({
        type: ROOM_DELETE_REQUEST,
        payload: roomId
    })
    try {
       
        Axios.delete(`/api/ROOMs/${roomId}`).catch((error)=>{console.log(error)})

        dispatch({
            type: ROOM_DELETE_SUCCESS
        })
    } catch (error) {
       
        dispatch({
            type: ROOM_DELETE_FAIL,
            payload: error.response && error.response.data.message
                ? error.response.data.message
                : error.message,

        })
    }
}
