
import {TENANT_LIST_REQUEST,TENANT_LIST_SUCCESS,TENANT_LIST_FAIL} from '../constants/tenantconstants'

export const tenantListReducer = (state={tenants:[],loading:true}, action) => {
    switch (action.type) {
        case TENANT_LIST_REQUEST:
            return { loading: true }

        case TENANT_LIST_SUCCESS:
            return {tenantList: action.payload }

        case TENANT_LIST_FAIL:
            return { loading: false, error: action.payload }
        default:
            return state;
    }
}