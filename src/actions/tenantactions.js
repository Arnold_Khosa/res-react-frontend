// import Axios from "axios"

import Axios from 'axios'
import { TENANT_DETAILS_REQUEST, TENANT_DETAILS_SUCCESS, TENANT_DETAILS_FAIL } from '../constants/tenantconstants'
import { TENANT_LIST_SUCCESS, TENANT_LIST_REQUEST, TENANT_LIST_FAIL } from '../constants/tenantconstants'
import { TENANT_DELETE_SUCCESS, TENANT_DELETE_REQUEST, TENANT_DELETE_FAIL } from '../constants/tenantconstants'
import { TENANT_EDIT_SUCCESS, TENANT_EDIT_REQUEST, TENANT_EDIT_FAIL } from '../constants/tenantconstants'
import { TENANT_ADD_SUCCESS, TENANT_ADD_REQUEST, TENANT_ADD_FAIL } from '../constants/tenantconstants'


export const listTenants = () => async (dispatch) => {
    dispatch({
        type: TENANT_LIST_REQUEST
    })
  
    try {
        var {data} = await Axios.get('/api/tenants')
     
       
        dispatch({
            type: TENANT_LIST_SUCCESS,
            payload: data.data
        })
    } catch (error) {
        
        dispatch({
            type: TENANT_LIST_FAIL,
            payload: error.message
        })
    }
}

export const addTenant = (tenant) => async (dispatch) => {
    dispatch({
        type: TENANT_ADD_REQUEST,
        payload:tenant
    })
  console.log(tenant)
  var data
    try {
        Axios.post('/api/tenants',tenant
        ).then(res=>{
      data=res;
        })
        dispatch({
            type: TENANT_ADD_SUCCESS,
            payload: data
        })
    } catch (error) {
     
        dispatch({
            type: TENANT_ADD_FAIL,
            payload: error.message
        })
    }
}

export const updateTenant = ({formData,id}) => async (dispatch) => {
    dispatch({
        type: TENANT_EDIT_REQUEST,
        payload: formData
    })
    try {
        Axios.patch(`/api/tenants/${id}`,formData).catch((error)=>{console.log(error)})

        dispatch({
            type: TENANT_EDIT_SUCCESS
        })
    } catch (error) {
       
        dispatch({
            type: TENANT_EDIT_FAIL,
            payload: error.response && error.response.data.message
                ? error.response.data.message
                : error.message,

        })
    }
}
export const deleteTenant = (tenantId) => async (dispatch) => {
    dispatch({
        type: TENANT_DELETE_REQUEST,
        payload: tenantId
    })
    try {
        console.log(tenantId)
        Axios.delete(`/api/tenants/${tenantId}`).catch((error)=>{console.log(error)})

        dispatch({
            type: TENANT_DELETE_SUCCESS
        })
    } catch (error) {
       
        dispatch({
            type: TENANT_DELETE_FAIL,
            payload: error.response && error.response.data.message
                ? error.response.data.message
                : error.message,

        })
    }
}

export const detailsTenant = (tenantId) => async (dispatch) => {
    dispatch({
        type: TENANT_DETAILS_REQUEST,
        payload: tenantId
    })
    try {
        const { data } = await Axios.get(`/api/tenants/${tenantId}`)

        dispatch({
            type: TENANT_DETAILS_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type: TENANT_DETAILS_FAIL,
            payload: error.response && error.response.data.message
                ? error.response.data.message
                : error.message,

        })
    }
}

