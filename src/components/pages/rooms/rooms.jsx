import React, { useState, useEffect } from "react"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faTrash, faEdit, faHome, faBed, faList, faPeopleArrows, faTable, faSearch } from '@fortawesome/free-solid-svg-icons';
import Pagination from "react-bootstrap-4-pagination"
import RoomsTable from "./roomstable";
import RoomsForm from "./roomform";
import SidebarToggler from "../../sidebartoggler"
import { useDispatch, useSelector } from 'react-redux'
import { deleteRoom,listRooms,addRoom,updateRoom } from "../../../actions/roomactions";
const Rooms = (props) => {
  const [modalShow, setModalShow] = React.useState(false);
  const [isUpdating, setIsUpdating] = useState(false)
  const [id,setId]=useState("");
  const handleAdd = () => {
    setIsUpdating(false)
    setModalShow(true)
  }
const dispatch= useDispatch();
  useEffect(()=>{
    dispatch(listRooms())

  },[])
  return (
    <>
      <div id="page-content-wrapper">
        <SidebarToggler />
        <div class="container-fluid">
        <div className="table-header ">
          <button onClick={handleAdd} className="btn addButton  mb-2 mt-2" id='addButton'>Add Room</button>
          <div className="search-bar"><input clasName="search" placeholder="Search" type="text" /></div>
        </div>
          <RoomsForm
            show={modalShow}
            onHide={() => setModalShow(false)}
            isUpdating={isUpdating}
            setIsUpdating={setIsUpdating}
            id={id}
          />
          <RoomsTable
            isUpdating={isUpdating}
            setIsUpdating={setIsUpdating}
            setModalShow={setModalShow}
            setId={setId}
         
          />
        </div>
      </div>
    </>
  )
}
export default Rooms;
