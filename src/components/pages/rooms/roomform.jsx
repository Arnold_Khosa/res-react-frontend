import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faWindowClose,faSav } from '@fortawesome/free-solid-svg-icons';
import { Link, useLocation } from "react-router-dom";
import { Modal, Button } from 'react-bootstrap';
import Input from "../../widgets/form/input";
import {useDispatch} from 'react-redux'
import {listRooms,addRoom,updateRoom } from "../../../actions/roomactions";
const RoomsForm=(props)=>{
    var {id}=props;
    const dispatch = useDispatch();
    const handleSubmit = (e) => {
        e.preventDefault();
        var formData = {}

        for (var i = 0; i < e.target.length; i++) {
            if (e.target[i].name !== "") {
                const name = e.target[i].name;
                const value = e.target[i].value;
                formData[name] = value;
            }
        }
        if (props.isUpdating === false) {
        dispatch(addRoom(formData))
        }
        else {
            dispatch(updateRoom({formData,id}))
         }
        dispatch(listRooms())
        props.onHide();
    }
    return (
        <>
                 <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        {props.isUpdating === true ? "Update Room Details" : "Add New Room"}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>

                    <form className="mb-2 me-3" onSubmit={ handleSubmit}>
                        <div className="form-row">
                            <Input
                                label={"Room Number"}
                                placeholder={"Room Number"}
                                name="roomNumber"
                                type="text"
                                required
                            />
                            <Input
                                label={"Room Key"}
                                placeholder={"Room Key"}
                                name="roomKey"
                                type="text"
                                required
                            />
                            <Input
                                label={"Room Price"}
                                placeholder={"Room Price"}
                                name="roomPrice"
                                type="number"
                                required
                            />
                             <Input
                                label={"Room Owner"}
                                placeholder={"Room Owner"}
                                name="roomOwner"
                                type="text"
                                required
                            />
                             <Input
                                label={"Room Type"}
                                placeholder={"Room Type"}
                                name="roomType"
                                type="text"
                                required
                            />
                             <Input
                                label={"Room Description"}
                                placeholder={"Room Description"}
                                name="roomDescription"
                                type="text"
                                required
                            />
                            <Input
                                label={"Room Status"}
                                placeholder={"Room Status"}
                                name="roomStatus"
                                type="text"
                                required
                            />
                        </div>
                        <Modal.Footer>
                    <button onClick={props.onHide} type="button" className="btn btn-danger">close </button>
                    <button type="submit" style={{backgroundColor:"#23282D"}} className="btn">Submit</button>
                </Modal.Footer>
                    </form>
                </Modal.Body>
               
            </Modal>

        </>
    )
}
export default RoomsForm;