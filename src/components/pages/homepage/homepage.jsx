import { BrowserRouter, BrowserRouter as Router, Link, NavLink, Route, Switch, Redirect } from "react-router-dom"
import { PieChart, Donut } from "react-minimal-pie-chart"
import React, { useState, useEffect } from "react"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import SidebarToggler from "../../sidebartoggler"
import { faBars, faTrash, faEdit, faUsers, faHome, faBed, faTools, faListAlt, faList, faPeopleArrows, faTable, faSearch } from '@fortawesome/free-solid-svg-icons';
import Pagination from "react-bootstrap-4-pagination";
import data from "../../data";


const Homepage = (props) => {
    return (
        <>
            <div class="home-container">
                <div className='first-row'>
                    <div className="small-block">
                        <span className="block-text">40</span>
                        <div className="block-label tenants"><FontAwesomeIcon icon={faUsers} /> Tenants</div>
                    </div>
                    <div className="small-block">
                        <span className="block-text">30</span>
                        <div className="block-label rooms"><FontAwesomeIcon icon={faBed} />  Rooms</div>
                    </div>
                    <div className="small-block">
                        <span className="block-text">45</span>
                        <div className="block-label waitlist"><FontAwesomeIcon icon={faList} /> Wait List </div>
                    </div>
                    <div className="small-block">
                        <span className="block-text">5</span>
                        <div className="block-label employees"><FontAwesomeIcon icon={faTools} /> Employees</div>
                    </div>
                </div>
                <div className="second-row">
                    <div className='first-column'>
                        <div className='inner-row-one'></div>
                        <div className='inner-row-two '></div>
                    </div>
                    <div className='second-column'>
                        <div className='inner-row-two'></div>
                        <div className='inner-row-one piechart'>
                            <PieChart data={
                                [
                                    { title: 'Tenants', value: 40, color: "#E38627" },
                                    { title: 'Employees', value: 50, color: "#EC13C7" },
                                    { title: 'WaitingList', value: 20, color: "#006eff" },
                                    { title: 'Rooms', value: 32, color: "#6A2135" }
                                ]
                            }
                                radius={43}
                                segmentsShift={1}
                                lineWidth={55}
                            >
                            </PieChart>

                        </div>
                    </div>
                </div>
            </div>

        </>
    )
}
export default Homepage;