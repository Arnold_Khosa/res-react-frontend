export const ROOM_LIST_REQUEST ='ROOM_LIST_REQUEST'
export const ROOM_LIST_SUCCESS ='ROOM_LIST_SUCCESS'
export const ROOM_LIST_FAIL ='ROOM_LIST_FAIL'

export const ROOM_DETAILS_REQUEST ='ROOM_DETAILS_REQUEST'
export const ROOM_DETAILS_SUCCESS ='ROOM_DETAILS_SUCCESS'
export const ROOM_DETAILS_FAIL ='ROOM_DETAILS_FAIL'

export const ROOM_DELETE_REQUEST ='ROOM_DELETE_REQUEST'
export const ROOM_DELETE_SUCCESS ='ROOM_DELETE_SUCCESS'
export const ROOM_DELETE_FAIL ='ROOM_DELETE_FAIL'


export const ROOM_EDIT_REQUEST ='ROOM_ADD_REQUEST'
export const ROOM_EDIT_SUCCESS ='ROOM_ADD_SUCCESS'
export const ROOM_EDIT_FAIL ='ROOM_ADD_FAIL'

export const ROOM_ADD_REQUEST ='ROOM_ADD_REQUEST'
export const ROOM_ADD_SUCCESS ='ROOM_ADD_SUCCESS'
export const ROOM_ADD_FAIL ='ROOM_ADD_FAIL'
