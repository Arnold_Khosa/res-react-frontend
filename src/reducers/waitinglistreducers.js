
import {WAITINGLIST_REQUEST,WAITINGLIST_SUCCESS,WAITINGLIST_FAIL} from '../constants/waitinglistconstants'
export const waitingListReducer = (state={waitingList:[],loading:true}, action) => {
    switch (action.type) {
        case WAITINGLIST_REQUEST:
            return { loading: true }

        case WAITINGLIST_SUCCESS:
            return {waitingList: action.payload }

        case WAITINGLIST_FAIL:
            return { loading: false, error: action.payload }
        default:
            return state;
    }
}
