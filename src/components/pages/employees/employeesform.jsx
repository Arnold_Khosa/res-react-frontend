import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faWindowClose, faSav } from '@fortawesome/free-solid-svg-icons';
import { Link, useLocation } from "react-router-dom";
import { Modal, Button } from 'react-bootstrap';
import Input from "../../widgets/form/input";
import { listEmployees,addEmployee,updateEmployee } from "../../../actions/employeeactions";
import { useDispatch, useSelector } from 'react-redux'
const EmployeesForm = (props) => {
    var {id}= props;
    const dispatch = useDispatch();
    const handleSubmit = (e) => {
        e.preventDefault();
        var formData = {}

        for (var i = 0; i < e.target.length; i++) {
            if (e.target[i].name !== "") {
                const name = e.target[i].name;
                const value = e.target[i].value;
                formData[name] = value;
            }
        }
        if (props.isUpdating === false) {
            dispatch(addEmployee(formData))
        }
        else {
            dispatch(updateEmployee({formData,id}))
        }
        dispatch(listEmployees())
        props.onHide();
    }
    return (
        <>
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        {props.isUpdating === true ? "Update Employee Details" : "Add New Employee"}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>

                    <form className="mb-2 me-3" onSubmit={handleSubmit}>
                        <div className="form-row">
                            <Input
                                label={"Name"}
                                placeholder={"Name"}
                                name="name"
                                type="text"
                                required
                            />
                            <Input
                                label={"Surname"}
                                placeholder={"Surname"}
                                name="surname"
                                type="text"
                                required
                            />
                            <Input
                                label={"Phone Number"}
                                placeholder={"phone Number"}
                                name="contact"
                                type="text"
                                required
                            />
                            <Input
                                label={"Date Hired"}
                                placeholder={"Date Hired"}
                                name="startDate"
                                type="date"
                                required
                            />
                            <Input
                                label={"Date Contract End"}
                                placeholder={"Date Contract End"}
                                name="endDate"
                                type="date"
                                required
                            />
                            <Input
                                label={"Job Description"}
                                placeholder={"Job Description"}
                                name="jobDescription"
                                type="text"
                                required
                            />
                            <Input
                                label={"Salary"}
                                placeholder={"Salary"}
                                name="salary"
                                type="number"
                                required
                            />
                        </div>
                        <Modal.Footer>
                            <Button onClick={props.onHide} type="button" className="btn btn-danger">close </Button>
                            <Button type="submit" style={{ backgroundColor: "#23282D" }} className="btn">Submit</Button>
                        </Modal.Footer>
                    </form>
                </Modal.Body>
            </Modal>
        </>
    )
}
export default EmployeesForm