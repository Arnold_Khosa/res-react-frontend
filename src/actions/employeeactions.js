// import Axios from "axios"

import Axios from 'axios'
import { EMPLOYEE_LIST_SUCCESS, EMPLOYEE_LIST_REQUEST, EMPLOYEE_LIST_FAIL } from '../constants/employeeconstants'
import { EMPLOYEE_DELETE_SUCCESS, EMPLOYEE_DELETE_REQUEST, EMPLOYEE_DELETE_FAIL } from '../constants/employeeconstants'
import { EMPLOYEE_EDIT_SUCCESS, EMPLOYEE_EDIT_REQUEST, EMPLOYEE_EDIT_FAIL } from '../constants/employeeconstants'
import { EMPLOYEE_ADD_SUCCESS, EMPLOYEE_ADD_REQUEST, EMPLOYEE_ADD_FAIL } from '../constants/employeeconstants'

export const listEmployees = () => async (dispatch) => {
    dispatch({
        type: EMPLOYEE_LIST_REQUEST
    })
  
    try {
        var {data} = await Axios.get('/api/employees')
     
       
        dispatch({
            type: EMPLOYEE_LIST_SUCCESS,
            payload: data.data
        })
    } catch (error) {
        
        dispatch({
            type: EMPLOYEE_LIST_FAIL,
            payload: error.message
        })
    }
}

export const addEmployee = (employee) => async (dispatch) => {
    dispatch({
        type: EMPLOYEE_ADD_REQUEST,
        payload:employee
    })
  console.log(employee)
  var data
    try {
        Axios.post('/api/Employees',employee
        ).then(res=>{
      data=res;
        })
        dispatch({
            type: EMPLOYEE_ADD_SUCCESS,
            payload: data
        })
    } catch (error) {
     
        dispatch({
            type: EMPLOYEE_ADD_FAIL,
            payload: error.message
        })
    }
}

export const updateEmployee = ({formData,id}) => async (dispatch) => {
    dispatch({
        type: EMPLOYEE_EDIT_REQUEST,
        payload: formData
    })
    try {
        Axios.patch(`/api/employees/${id}`,formData).catch((error)=>{console.log(error)})

        dispatch({
            type: EMPLOYEE_EDIT_SUCCESS
        })
    } catch (error) {
       
        dispatch({
            type: EMPLOYEE_EDIT_FAIL,
            payload: error.response && error.response.data.message
                ? error.response.data.message
                : error.message,

        })
    }
}
export const deleteEmployee = (employeeId) => async (dispatch) => {
    dispatch({
        type: EMPLOYEE_DELETE_REQUEST,
        payload: employeeId
    })
    try {
        console.log(employeeId)
        Axios.delete(`/api/EMPLOYEEs/${employeeId}`).catch((error)=>{console.log(error)})

        dispatch({
            type: EMPLOYEE_DELETE_SUCCESS
        })
    } catch (error) {
       
        dispatch({
            type: EMPLOYEE_DELETE_FAIL,
            payload: error.response && error.response.data.message
                ? error.response.data.message
                : error.message,

        })
    }
}
