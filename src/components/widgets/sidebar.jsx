import React, { useState, useEffect } from "react"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faTrash, faEdit, faHome, faBed, faUser, faList, faPeopleArrows, faTable, faSearch } from '@fortawesome/free-solid-svg-icons';
import Pagination from "react-bootstrap-4-pagination"
import { Link } from "react-router-dom"

const Sidebar = () => {
    return (
        <>
            <div style={{ backgroundColor: "#23282D", width: "280px" }} className="sidebar">
                <div className="header d-flex pt-3 ps-3">
                    <i className="header d-flex align-items-center justify-content-center"><FontAwesomeIcon icon={faUser} /></i>
                    <span className=" mt-3 mb-2 me-2">Arnold Khosa</span>
                </div>
                <div className="input-group container mb-2 mt-2">
                </div>
                <hr />
                <nav className="navbar">
                    <ul className="nav d-flex flex-column">
                        <li className="nav-item"><Link to="/" className="link"><i className="me-3"><FontAwesomeIcon icon={faHome} /></i>Home</Link></li>
                        <li className="nav-item"><Link to="/api/tenants" className="link"><i className="me-3"><FontAwesomeIcon icon={faPeopleArrows} /></i>Tenants</Link></li>
                        <li className="nav-item"><Link to="/api/rooms" className="link"><i className="me-3"><FontAwesomeIcon icon={faBed} /></i>Rooms</Link></li>
                        <li className="nav-item"><Link to="/api/employees" className="link"><i className="me-3"><FontAwesomeIcon icon={faBed} /></i>Employees</Link></li>
                        <li className="nav-item"><Link to="/api/waitinglist" className="link"><i className="me-3"><FontAwesomeIcon icon={faBed} /></i>Waiting List</Link></li>
                    </ul>
                </nav>
            </div>
        </>
    )
}
export default Sidebar;