import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import "bootstrap/dist/js/bootstrap.bundle.min.js"
import "bootstrap/dist/css/bootstrap.min.css"
import { Provider } from 'react-redux';
import "./css/styles.css"
import "./js/scripts.js"

 import store from './store.js'

ReactDOM.render(
  <Provider store={store}>
  <React.StrictMode>
    <App />
  </React.StrictMode>
  </Provider>
  ,
  document.getElementById('root')
);

