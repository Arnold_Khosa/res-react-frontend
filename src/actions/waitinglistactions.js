// import Axios from "axios"

import Axios from 'axios'
import { WAITINGLIST_DETAILS_REQUEST, WAITINGLIST_DETAILS_SUCCESS, WAITINGLIST_DETAILS_FAIL } from '../constants/waitinglistconstants'
import { WAITINGLIST_SUCCESS, WAITINGLIST_REQUEST, WAITINGLIST_FAIL } from '../constants/waitinglistconstants'
import { WAITINGLIST_DELETE_SUCCESS, WAITINGLIST_DELETE_REQUEST, WAITINGLIST_DELETE_FAIL } from '../constants/waitinglistconstants'
import { WAITINGLIST_EDIT_SUCCESS, WAITINGLIST_EDIT_REQUEST, WAITINGLIST_EDIT_FAIL } from '../constants/waitinglistconstants'
import { WAITINGLIST_ADD_SUCCESS, WAITINGLIST_ADD_REQUEST, WAITINGLIST_ADD_FAIL } from '../constants/waitinglistconstants'


export const listWaitingList = () => async (dispatch) => {
    dispatch({
        type: WAITINGLIST_REQUEST
    })
  
    try {
        var {data} = await Axios.get('/api/waitinglist')
     
       
        dispatch({
            type: WAITINGLIST_SUCCESS,
            payload: data.data
        })
    } catch (error) {
        
        dispatch({
            type: WAITINGLIST_FAIL,
            payload: error.message
        })
    }
}

export const addToWaitingList = (listItem) => async (dispatch) => {
    dispatch({
        type: WAITINGLIST_ADD_REQUEST,
        payload:listItem
    })
  console.log(listItem)
  var data
    try {
        Axios.post('/api/waitinglist',listItem
        ).then(res=>{
      data=res;
        })
        dispatch({
            type: WAITINGLIST_ADD_SUCCESS,
            payload: data
        })
    } catch (error) {
     
        dispatch({
            type: WAITINGLIST_ADD_FAIL,
            payload: error.message
        })
    }
}

export const updateWaitingListItem = ({formData,id}) => async (dispatch) => {
    dispatch({
        type: WAITINGLIST_EDIT_REQUEST,
        payload: formData
    })
    try {
        Axios.patch(`/api/waitinglist/${id}`,formData).catch((error)=>{console.log(error)})

        dispatch({
            type: WAITINGLIST_EDIT_SUCCESS
        })
    } catch (error) {
       
        dispatch({
            type: WAITINGLIST_EDIT_FAIL,
            payload: error.response && error.response.data.message
                ? error.response.data.message
                : error.message,

        })
    }
}
export const deleteWaitingListItem = (listItemId) => async (dispatch) => {
    dispatch({
        type: WAITINGLIST_DELETE_REQUEST,
        payload: listItemId
    })
    try {
        console.log(listItemId)
        Axios.delete(`/api/waitinglist/${listItemId}`).catch((error)=>{console.log(error)})

        dispatch({
            type: WAITINGLIST_DELETE_SUCCESS
        })
    } catch (error) {
       
        dispatch({
            type: WAITINGLIST_DELETE_FAIL,
            payload: error.response && error.response.data.message
                ? error.response.data.message
                : error.message,

        })
    }
}