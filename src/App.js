import { BrowserRouter, BrowserRouter as Router, Route, Switch } from "react-router-dom"
import React from "react"

import Tenants from "./components/pages/tenants/tenants"
import Homepage from "./components/pages/homepage/homepage"
import Rooms from "./components/pages/rooms/rooms"
import Waitinglist from "./components/pages/waitinglist/Waitinglist"
import Employees from "./components/pages/employees/employees"
import Login from "./components/pages/login/login"
import Error from "./components/pages/errorpage/error"
import { useDispatch, useSelector } from 'react-redux'
import PrivateRoute from "./components/auth"
import "./css/styles.css"
import Axios from "axios"
import axios from "axios"
import jwt_decode from "jwt-decode";
import { refreshToken } from "./actions/useractions";
import Sidebar from "./components/sideBar"
 
const App = () => {
    const { userInfo } = useSelector((state) => state.userSignin)
    const dispatch= useDispatch();
    Axios.interceptors.request.use(
        async config => {
            let currentDate = new Date();
           const userInfo_=JSON.parse(localStorage.getItem('userInfo'))
            if (userInfo_) {
                const decodedToken = jwt_decode(userInfo_.accessToken)
                const accessToken = userInfo_.accessToken
                config.headers["auth"] = `Bearer ${accessToken}`
                
                if (decodedToken.exp * 1000 < currentDate.getTime()) {
                    console.log('Expired',userInfo_.refreshToken)
                    dispatch(refreshToken({token:userInfo_.refreshToken}))
                }
                config.headers['auth'] =  'Bearer '+JSON.parse(localStorage.getItem('userInfo')).accessToken
            }
            return config;
        },
        error => {
            console.log(error)
            Promise.reject(error)
        });
    axios.interceptors.response.use(async (response) => {

        return response
    }, async function (error) {
        const originalRequest = error.config;
        if (error.response.status === 401 && !originalRequest._retry) {
            originalRequest._retry = true;

            return Axios(originalRequest);
        }
        if (error.response.status === 408 ) {
           localStorage.removeItem('userInfo');

        }
        return Promise.reject(error);
    });
  
    return (
        <> <Router>

            <div className="main-container ">
                {userInfo && !userInfo.timeout ? <Sidebar /> : null}
                <main className="wrapper" id="wrapper">
                    <Switch>
                        <PrivateRoute exact path="/">
                            <Homepage />
                        </PrivateRoute>
                        <Route exact path="/login">
                            <Login />
                        </Route>

                        <PrivateRoute path="/api/Tenants">
                            <Tenants />
                        </PrivateRoute>

                        <PrivateRoute path="/api/rooms">
                            <Rooms />
                        </PrivateRoute>

                        <PrivateRoute path="/api/employees">
                            <Employees />
                        </PrivateRoute>

                        <PrivateRoute path="/api/waitinglist" >
                            <Waitinglist />
                        </PrivateRoute>

                        <Route path="*">
                            <Error />
                        </Route>
                    </Switch>
                </main>
            </div>
        </Router>
        </>
    )
}

export default App;
