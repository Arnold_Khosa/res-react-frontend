import { BrowserRouter, BrowserRouter as Router, Link, NavLink, Route, Switch } from "react-router-dom"
import React, { useState, useEffect } from "react"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useDispatch, useSelector } from 'react-redux'
import { faSignOutAlt, faCaretDown, faUser, faBars, faTimes } from '@fortawesome/free-solid-svg-icons';
import { signOut } from "../actions/useractions";

const SideBar = (props) => {
    const { userInfo } = useSelector((state) => state.userSignin)
    const[navState,setNavState] = useState(true);
    const dispatch = useDispatch()
    const signOutHandler = () => {
        dispatch(signOut())
    }
   
    return (
        <>
            <header class="navbar-expand-md">
                <button type='button' onClick={()=>{setNavState(!navState)}} style={{ fontSize: "30px", padding: "10px" }} className="toggle" data-toggle="collapse" data-target="#collapsibleNavbar">
                    <span class="navbar-toggler-button"><FontAwesomeIcon icon={navState? faBars: faTimes } /></span>
                </button>
                <div class=" topnav collapse navbar-collapse" id="collapsibleNavbar">
                    <NavLink exact to="/" className="link" activeClassName="active">Home</NavLink>
                    <NavLink to="/api/tenants" className="link" activeClassName="active">Tenants</NavLink>
                    <NavLink to="/api/rooms" className="link" activeClassName="active">Rooms</NavLink>
                    <NavLink to="/api/employees" className="link" activeClassName="active">Employees</NavLink>
                    <NavLink to="/api/waitinglist" className="link" activeClassName="active">Waiting List</NavLink>
                    {/* <span className="current-user">{userInfo.username}<i><FontAwesomeIcon icon={faUser}/></i></span> */}
                    <span className="dropdown current-user">
                        {userInfo ? userInfo.username : ''} <FontAwesomeIcon icon={faCaretDown} />
                        <div className={userInfo ? 'dropdown-content profile' : ''}>
                            <NavLink className="profile-link" onClick={signOutHandler} to='/profile'> <FontAwesomeIcon icon={faUser} />Profile</NavLink>
                        </div>
                        <div className={userInfo ? 'dropdown-content logout' : ''}>
                            <NavLink className="logout-link" onClick={signOutHandler} to='/'> <FontAwesomeIcon icon={faSignOutAlt} />Sign Out</NavLink>
                        </div>
                    </span>
                </div>
            </header>
        </>
    )
}
export default SideBar;